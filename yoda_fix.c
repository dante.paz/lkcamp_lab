/*
 * Yoda's char device driver
 *
 * Copyright (C) 2013 Ezequiel Garcia.
 *
 * GPL applies: you know what that means.
 */

#define pr_fmt(fmt) KBUILD_MODNAME ": " fmt

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/uaccess.h>

static size_t ava=200;
static int major;
char *yoda_string; 
static ssize_t yoda_read_buggy(struct file *file, char __user *buf,
			       size_t count, loff_t *offset)
{
	if((*offset >= (loff_t) strlen(yoda_string)))
	{
		return(0);
	}
	else
	{
		if (copy_to_user(buf, &yoda_string[*offset], 1))
			return -EFAULT;
		(*offset)++;
		return (1);
	}
}

static ssize_t yoda_write(struct file *file, const char __user *buf,
			       size_t count, loff_t *offset)
{

      	pr_info("user wants to write %zd bytes at %lld\n", count, *offset);
	count=simple_write_to_buffer(yoda_string, ava, offset,buf, count);
	return(count);
}


static const struct file_operations fops = {
	.read = yoda_read_buggy,
	.write = yoda_write,
};

static int yoda_init(void)
{
	yoda_string=vmalloc(sizeof(char)*ava);
	major = register_chrdev(0, KBUILD_MODNAME, &fops);
	if (major < 0)
		return major;

	pr_info("device registered with major %d\n", major);
	return 0;
}

static void yoda_exit(void)
{
	return unregister_chrdev(major, KBUILD_MODNAME);
}

module_init(yoda_init);
module_exit(yoda_exit);

MODULE_AUTHOR("Ezequiel Garcia");
MODULE_DESCRIPTION("A simple read-only char device driver");
MODULE_LICENSE("GPL");
